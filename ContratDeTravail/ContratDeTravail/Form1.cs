﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using System;
using System.IO;
using System.Windows.Forms;



namespace ContratDeTravail
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        #region Interface Employeur
        #region Interface Employeur1

        public static string nomResp1;
        private void Nom_responsable1_KeyPress(object sender, KeyPressEventArgs e)
        {
            Utilisation.Anti_chiffre(e);
            nomResp1 = nom_responsable1.Text;
        }

        public static string prenomResp1;
        private void Prenom_responsable1_KeyPress(object sender, KeyPressEventArgs e)
        {
            Utilisation.Anti_chiffre(e);
            prenomResp1 = prenom_responsable1.Text;
        }
        
        public static string adresseResp1;
        private void adresse_responsable1_TextChanged(object sender, EventArgs e)
        {
            adresseResp1 = adresse_responsable1.Text;
        }

        public static string telFixeResp1;
        private void Telfix_responsable1_KeyPress(object sender, KeyPressEventArgs e)
        {
            Utilisation.Anti_lettre(e);
            telFixeResp1 = telfix_responsable1.Text;
        }

        public static string portResp1;
        private void Port_responsable1_KeyPress(object sender, KeyPressEventArgs e)
        {
            Utilisation.Anti_lettre(e);
            portResp1 = port_responsable1.Text;
        }

        public static string telTafResp1;
        private void Teltaf_responsable1_KeyPress(object sender, KeyPressEventArgs e)
        {
            Utilisation.Anti_lettre(e);
            telTafResp1 = teltaf_responsable1.Text;
        }

        #endregion

        #region Interface Employeur2

        public static string nomResp2;
        private void Nom_responsable2_KeyPress(object sender, KeyPressEventArgs e)
        {
            Utilisation.Anti_chiffre(e);
        }

        public static string prenomResp2;
        private void Prenom_responsable2_KeyPress(object sender, KeyPressEventArgs e)
        {
            Utilisation.Anti_chiffre(e);
        }

        public static string adresseResp2;
        private void adresse_responsable2_TextChanged(object sender, EventArgs e)
        {

        }

        public static string telFixeResp2;
        private void Telfix_responsable2_KeyPress(object sender, KeyPressEventArgs e)
        {
            Utilisation.Anti_lettre(e);
        }

        public static string portableResp2;
        private void Port_responsable2_KeyPress(object sender, KeyPressEventArgs e)
        {
            Utilisation.Anti_lettre(e);
        }

        public static string telTafResp2;
        private void Teltaf_responsable2_KeyPress(object sender, KeyPressEventArgs e)
        {
            Utilisation.Anti_lettre(e);
        }

        #endregion

        #endregion

        #region Interface Enfant

        public static string nomEnfant;
        private void Nom_enfant_KeyPress(object sender, KeyPressEventArgs e)
        {
            Utilisation.Anti_chiffre(e);
            nomEnfant = nom_enfant.Text;
        }

        public static string prenomEnfant;
        private void Prenom_enfant_KeyPress(object sender, KeyPressEventArgs e)
        {
            Utilisation.Anti_chiffre(e);
            prenomEnfant = prenom_enfant.Text;
        }

        public static string datNaissEnfant;
        private void dateNaiss_enfant_TextChanged(object sender, EventArgs e)
        {
            datNaissEnfant = dateNaiss_enfant.Text;
        }
        #endregion

        #region Interface contrat
        private void Nb_jour_essai_ValueChanged(object sender, EventArgs e)
        {

            nb_jour_essai.Maximum = 7;
            nb_jour_essai.Minimum = 1;

            Utilisation.Verif_durer_essai(nb_jour_essai);

        }
        #endregion
        #region Pdf
        /// <summary>
        /// Bouton pour importer un pdf (Ne sert a rien... ) 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonImporter_Click(object sender, EventArgs e)
        {

        }
        /// <summary>
        /// Bouton pour Actualiser les écritures entre le fichier et la RicheTextBox (Sert a rien ... ) 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void bt_Actualiser_Click(object sender, EventArgs e)
        {
            
        }
        /// <summary>
        /// Crée le fichier pdf
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void savePDF_Click(object sender, EventArgs e)
        {
            PdfBase.PdfBase1();
        }
        #endregion

    }
}
