﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ContratDeTravail
{
    public class Utilisation
    {
        /// <summary>
        /// Supprime l'acces au touche alphabétique 
        /// </summary>
        /// <param name="e"></param>
        public static void Anti_lettre(KeyPressEventArgs e)
        {
            if (char.IsNumber(e.KeyChar))
                e.Handled = false;
            else if (char.IsControl(e.KeyChar))
                e.Handled = false;
            else
                e.Handled = true;
        }
        /// <summary>
        /// Supprime l'acces au touche numérique
        /// </summary>
        /// <param name="e"></param>
        public static void Anti_chiffre(KeyPressEventArgs e)
        {
            if (char.IsNumber(e.KeyChar))
                e.Handled = true;
            else if (char.IsControl(e.KeyChar))
                e.Handled = false;
            else
                e.Handled = false;
        }
        /// <summary>
        /// Verifie la durer en mois de la periode d'essai en fonction du nombre d'accueil par semaine
        /// </summary>
        /// <param name="nb_jour_essai"></param>
        /// <returns></returns>
        public static bool Verif_durer_essai(NumericUpDown nb_jour_essai)
        {
            int nb_jour;
            bool longPeriodeEssai = true;

            nb_jour = Convert.ToInt32(nb_jour_essai.Value);

            if (nb_jour <= 3)
            {
                longPeriodeEssai = false;
            }

            return longPeriodeEssai;
        }
        
    }
}
