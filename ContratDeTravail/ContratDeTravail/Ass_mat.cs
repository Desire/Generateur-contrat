﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContratDeTravail
{
    class Ass_mat:Personne
    {
        private string adresse;
        private string tel_fixe;
        private string portable;
        
        public string Adresse { get => adresse; set => adresse = value; }
        public string Tel_fixe { get => tel_fixe; set => tel_fixe = value; }
        public string Portable { get => portable; set => portable = value; }

        /// <summary>
        /// Constructeur par defaut d'une Ass_mat
        /// </summary>
        public Ass_mat() 
            : base()
        {

        }

        public Ass_mat(string nom, string prenom, string adresse, string tel_fixe, string portable)
            :base()
        {
            Nom = nom;
            Prenom = prenom;
            this.Adresse = adresse;
            this.Tel_fixe = tel_fixe;
            this.Portable = portable;
        }

    }
}
