﻿namespace ContratDeTravail
{
    partial class Form1
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.onglet_Modification = new System.Windows.Forms.TabPage();
            this.dateNaiss_enfant = new System.Windows.Forms.TextBox();
            this.prenom_enfant = new System.Windows.Forms.TextBox();
            this.nom_enfant = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.onglet_Employeur = new System.Windows.Forms.TabPage();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tab_Responsable1 = new System.Windows.Forms.TabPage();
            this.teltaf_responsable1 = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.port_responsable1 = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.telfix_responsable1 = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.adresse_responsable1 = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.prenom_responsable1 = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.nom_responsable1 = new System.Windows.Forms.TextBox();
            this.tab_Responsable2 = new System.Windows.Forms.TabPage();
            this.teltaf_responsable2 = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.port_responsable2 = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.telfix_responsable2 = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.adresse_responsable2 = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.prenom_responsable2 = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.nom_responsable2 = new System.Windows.Forms.TextBox();
            this.ongletEmployeur = new System.Windows.Forms.TabControl();
            this.Périodedessai = new System.Windows.Forms.TabPage();
            this.label15 = new System.Windows.Forms.Label();
            this.cal_essai_debut = new System.Windows.Forms.MonthCalendar();
            this.Contrat = new System.Windows.Forms.TabPage();
            this.End = new System.Windows.Forms.TabPage();
            this.buttonImporter = new System.Windows.Forms.Button();
            this.bt_Actualiser = new System.Windows.Forms.Button();
            this.savePDF = new System.Windows.Forms.Button();
            this.lookPDF = new System.Windows.Forms.RichTextBox();
            this.nb_jour_essai = new System.Windows.Forms.NumericUpDown();
            this.label14 = new System.Windows.Forms.Label();
            this.onglet_Modification.SuspendLayout();
            this.onglet_Employeur.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tab_Responsable1.SuspendLayout();
            this.tab_Responsable2.SuspendLayout();
            this.ongletEmployeur.SuspendLayout();
            this.Périodedessai.SuspendLayout();
            this.Contrat.SuspendLayout();
            this.End.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nb_jour_essai)).BeginInit();
            this.SuspendLayout();
            // 
            // onglet_Modification
            // 
            this.onglet_Modification.Controls.Add(this.dateNaiss_enfant);
            this.onglet_Modification.Controls.Add(this.prenom_enfant);
            this.onglet_Modification.Controls.Add(this.nom_enfant);
            this.onglet_Modification.Controls.Add(this.label13);
            this.onglet_Modification.Controls.Add(this.label11);
            this.onglet_Modification.Controls.Add(this.label12);
            this.onglet_Modification.Location = new System.Drawing.Point(4, 22);
            this.onglet_Modification.Name = "onglet_Modification";
            this.onglet_Modification.Padding = new System.Windows.Forms.Padding(3);
            this.onglet_Modification.Size = new System.Drawing.Size(606, 424);
            this.onglet_Modification.TabIndex = 2;
            this.onglet_Modification.Text = "Enfant";
            this.onglet_Modification.UseVisualStyleBackColor = true;
            // 
            // dateNaiss_enfant
            // 
            this.dateNaiss_enfant.Location = new System.Drawing.Point(28, 123);
            this.dateNaiss_enfant.Name = "dateNaiss_enfant";
            this.dateNaiss_enfant.Size = new System.Drawing.Size(152, 20);
            this.dateNaiss_enfant.TabIndex = 9;
            this.dateNaiss_enfant.TextChanged += new System.EventHandler(this.dateNaiss_enfant_TextChanged);
            // 
            // prenom_enfant
            // 
            this.prenom_enfant.Location = new System.Drawing.Point(28, 80);
            this.prenom_enfant.Name = "prenom_enfant";
            this.prenom_enfant.Size = new System.Drawing.Size(152, 20);
            this.prenom_enfant.TabIndex = 7;
            this.prenom_enfant.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Prenom_enfant_KeyPress);
            // 
            // nom_enfant
            // 
            this.nom_enfant.Location = new System.Drawing.Point(28, 41);
            this.nom_enfant.Name = "nom_enfant";
            this.nom_enfant.Size = new System.Drawing.Size(152, 20);
            this.nom_enfant.TabIndex = 4;
            this.nom_enfant.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Nom_enfant_KeyPress);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(25, 107);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(102, 13);
            this.label13.TabIndex = 8;
            this.label13.Text = "Date de naissance :";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(25, 64);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(49, 13);
            this.label11.TabIndex = 6;
            this.label11.Text = "Prenom :";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(25, 25);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(35, 13);
            this.label12.TabIndex = 5;
            this.label12.Text = "Nom :";
            // 
            // onglet_Employeur
            // 
            this.onglet_Employeur.Controls.Add(this.tabControl1);
            this.onglet_Employeur.Location = new System.Drawing.Point(4, 22);
            this.onglet_Employeur.Name = "onglet_Employeur";
            this.onglet_Employeur.Padding = new System.Windows.Forms.Padding(3);
            this.onglet_Employeur.Size = new System.Drawing.Size(606, 424);
            this.onglet_Employeur.TabIndex = 0;
            this.onglet_Employeur.Text = "Employeur";
            this.onglet_Employeur.UseVisualStyleBackColor = true;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tab_Responsable1);
            this.tabControl1.Controls.Add(this.tab_Responsable2);
            this.tabControl1.Location = new System.Drawing.Point(3, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(600, 418);
            this.tabControl1.TabIndex = 0;
            // 
            // tab_Responsable1
            // 
            this.tab_Responsable1.Controls.Add(this.teltaf_responsable1);
            this.tab_Responsable1.Controls.Add(this.label17);
            this.tab_Responsable1.Controls.Add(this.port_responsable1);
            this.tab_Responsable1.Controls.Add(this.label5);
            this.tab_Responsable1.Controls.Add(this.telfix_responsable1);
            this.tab_Responsable1.Controls.Add(this.label4);
            this.tab_Responsable1.Controls.Add(this.adresse_responsable1);
            this.tab_Responsable1.Controls.Add(this.label3);
            this.tab_Responsable1.Controls.Add(this.prenom_responsable1);
            this.tab_Responsable1.Controls.Add(this.label2);
            this.tab_Responsable1.Controls.Add(this.label1);
            this.tab_Responsable1.Controls.Add(this.nom_responsable1);
            this.tab_Responsable1.Location = new System.Drawing.Point(4, 22);
            this.tab_Responsable1.Name = "tab_Responsable1";
            this.tab_Responsable1.Padding = new System.Windows.Forms.Padding(3);
            this.tab_Responsable1.Size = new System.Drawing.Size(592, 392);
            this.tab_Responsable1.TabIndex = 0;
            this.tab_Responsable1.Text = "Responsable (employeur)";
            this.tab_Responsable1.UseVisualStyleBackColor = true;
            // 
            // teltaf_responsable1
            // 
            this.teltaf_responsable1.Location = new System.Drawing.Point(25, 283);
            this.teltaf_responsable1.Name = "teltaf_responsable1";
            this.teltaf_responsable1.Size = new System.Drawing.Size(152, 20);
            this.teltaf_responsable1.TabIndex = 11;
            this.teltaf_responsable1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Teltaf_responsable1_KeyPress);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(22, 267);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(95, 13);
            this.label17.TabIndex = 10;
            this.label17.Text = "Téléphone travail :";
            // 
            // port_responsable1
            // 
            this.port_responsable1.Location = new System.Drawing.Point(25, 244);
            this.port_responsable1.Name = "port_responsable1";
            this.port_responsable1.Size = new System.Drawing.Size(152, 20);
            this.port_responsable1.TabIndex = 9;
            this.port_responsable1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Port_responsable1_KeyPress);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(22, 228);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(52, 13);
            this.label5.TabIndex = 8;
            this.label5.Text = "Portable :";
            // 
            // telfix_responsable1
            // 
            this.telfix_responsable1.Location = new System.Drawing.Point(25, 205);
            this.telfix_responsable1.Name = "telfix_responsable1";
            this.telfix_responsable1.Size = new System.Drawing.Size(152, 20);
            this.telfix_responsable1.TabIndex = 7;
            this.telfix_responsable1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Telfix_responsable1_KeyPress);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(22, 189);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(83, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "Telephone fixe :";
            // 
            // adresse_responsable1
            // 
            this.adresse_responsable1.Location = new System.Drawing.Point(25, 140);
            this.adresse_responsable1.Name = "adresse_responsable1";
            this.adresse_responsable1.Size = new System.Drawing.Size(152, 20);
            this.adresse_responsable1.TabIndex = 5;
            this.adresse_responsable1.TextChanged += new System.EventHandler(this.adresse_responsable1_TextChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(22, 124);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(51, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Adresse :";
            // 
            // prenom_responsable1
            // 
            this.prenom_responsable1.Location = new System.Drawing.Point(25, 71);
            this.prenom_responsable1.Name = "prenom_responsable1";
            this.prenom_responsable1.Size = new System.Drawing.Size(152, 20);
            this.prenom_responsable1.TabIndex = 3;
            this.prenom_responsable1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Prenom_responsable1_KeyPress);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(22, 55);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(49, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Prenom :";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(22, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Nom :";
            // 
            // nom_responsable1
            // 
            this.nom_responsable1.Location = new System.Drawing.Point(25, 32);
            this.nom_responsable1.Name = "nom_responsable1";
            this.nom_responsable1.Size = new System.Drawing.Size(152, 20);
            this.nom_responsable1.TabIndex = 0;
            this.nom_responsable1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Nom_responsable1_KeyPress);
            // 
            // tab_Responsable2
            // 
            this.tab_Responsable2.Controls.Add(this.teltaf_responsable2);
            this.tab_Responsable2.Controls.Add(this.label18);
            this.tab_Responsable2.Controls.Add(this.port_responsable2);
            this.tab_Responsable2.Controls.Add(this.label6);
            this.tab_Responsable2.Controls.Add(this.telfix_responsable2);
            this.tab_Responsable2.Controls.Add(this.label7);
            this.tab_Responsable2.Controls.Add(this.adresse_responsable2);
            this.tab_Responsable2.Controls.Add(this.label8);
            this.tab_Responsable2.Controls.Add(this.prenom_responsable2);
            this.tab_Responsable2.Controls.Add(this.label9);
            this.tab_Responsable2.Controls.Add(this.label10);
            this.tab_Responsable2.Controls.Add(this.nom_responsable2);
            this.tab_Responsable2.Location = new System.Drawing.Point(4, 22);
            this.tab_Responsable2.Name = "tab_Responsable2";
            this.tab_Responsable2.Padding = new System.Windows.Forms.Padding(3);
            this.tab_Responsable2.Size = new System.Drawing.Size(592, 392);
            this.tab_Responsable2.TabIndex = 1;
            this.tab_Responsable2.Text = "Responsable (non employeur)";
            this.tab_Responsable2.UseVisualStyleBackColor = true;
            // 
            // teltaf_responsable2
            // 
            this.teltaf_responsable2.Location = new System.Drawing.Point(25, 284);
            this.teltaf_responsable2.Name = "teltaf_responsable2";
            this.teltaf_responsable2.Size = new System.Drawing.Size(152, 20);
            this.teltaf_responsable2.TabIndex = 21;
            this.teltaf_responsable2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Teltaf_responsable2_KeyPress);
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(22, 268);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(95, 13);
            this.label18.TabIndex = 20;
            this.label18.Text = "Téléphone travail :";
            // 
            // port_responsable2
            // 
            this.port_responsable2.Location = new System.Drawing.Point(25, 245);
            this.port_responsable2.Name = "port_responsable2";
            this.port_responsable2.Size = new System.Drawing.Size(152, 20);
            this.port_responsable2.TabIndex = 19;
            this.port_responsable2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Port_responsable2_KeyPress);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(22, 229);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(52, 13);
            this.label6.TabIndex = 18;
            this.label6.Text = "Portable :";
            // 
            // telfix_responsable2
            // 
            this.telfix_responsable2.Location = new System.Drawing.Point(25, 206);
            this.telfix_responsable2.Name = "telfix_responsable2";
            this.telfix_responsable2.Size = new System.Drawing.Size(152, 20);
            this.telfix_responsable2.TabIndex = 17;
            this.telfix_responsable2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Telfix_responsable2_KeyPress);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(22, 190);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(83, 13);
            this.label7.TabIndex = 16;
            this.label7.Text = "Telephone fixe :";
            // 
            // adresse_responsable2
            // 
            this.adresse_responsable2.Location = new System.Drawing.Point(25, 141);
            this.adresse_responsable2.Name = "adresse_responsable2";
            this.adresse_responsable2.Size = new System.Drawing.Size(152, 20);
            this.adresse_responsable2.TabIndex = 15;
            this.adresse_responsable2.TextChanged += new System.EventHandler(this.adresse_responsable2_TextChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(22, 125);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(51, 13);
            this.label8.TabIndex = 14;
            this.label8.Text = "Adresse :";
            // 
            // prenom_responsable2
            // 
            this.prenom_responsable2.Location = new System.Drawing.Point(25, 72);
            this.prenom_responsable2.Name = "prenom_responsable2";
            this.prenom_responsable2.Size = new System.Drawing.Size(152, 20);
            this.prenom_responsable2.TabIndex = 13;
            this.prenom_responsable2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Prenom_responsable2_KeyPress);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(22, 56);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(49, 13);
            this.label9.TabIndex = 12;
            this.label9.Text = "Prenom :";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(22, 17);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(35, 13);
            this.label10.TabIndex = 11;
            this.label10.Text = "Nom :";
            // 
            // nom_responsable2
            // 
            this.nom_responsable2.Location = new System.Drawing.Point(25, 33);
            this.nom_responsable2.Name = "nom_responsable2";
            this.nom_responsable2.Size = new System.Drawing.Size(152, 20);
            this.nom_responsable2.TabIndex = 10;
            this.nom_responsable2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Nom_responsable2_KeyPress);
            // 
            // ongletEmployeur
            // 
            this.ongletEmployeur.Controls.Add(this.onglet_Employeur);
            this.ongletEmployeur.Controls.Add(this.onglet_Modification);
            this.ongletEmployeur.Controls.Add(this.Périodedessai);
            this.ongletEmployeur.Controls.Add(this.Contrat);
            this.ongletEmployeur.Controls.Add(this.End);
            this.ongletEmployeur.Location = new System.Drawing.Point(-2, 0);
            this.ongletEmployeur.Name = "ongletEmployeur";
            this.ongletEmployeur.SelectedIndex = 0;
            this.ongletEmployeur.Size = new System.Drawing.Size(614, 450);
            this.ongletEmployeur.TabIndex = 1;
            // 
            // Périodedessai
            // 
            this.Périodedessai.Controls.Add(this.label15);
            this.Périodedessai.Controls.Add(this.cal_essai_debut);
            this.Périodedessai.Location = new System.Drawing.Point(4, 22);
            this.Périodedessai.Name = "Périodedessai";
            this.Périodedessai.Padding = new System.Windows.Forms.Padding(3);
            this.Périodedessai.Size = new System.Drawing.Size(606, 424);
            this.Périodedessai.TabIndex = 3;
            this.Périodedessai.Text = "Période d\'essai";
            this.Périodedessai.UseVisualStyleBackColor = true;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(15, 12);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(139, 13);
            this.label15.TabIndex = 4;
            this.label15.Text = "Debut de la Période d\'éssai:";
            // 
            // cal_essai_debut
            // 
            this.cal_essai_debut.FirstDayOfWeek = System.Windows.Forms.Day.Monday;
            this.cal_essai_debut.Location = new System.Drawing.Point(166, 12);
            this.cal_essai_debut.Name = "cal_essai_debut";
            this.cal_essai_debut.ShowWeekNumbers = true;
            this.cal_essai_debut.TabIndex = 0;
            // 
            // Contrat
            // 
            this.Contrat.Controls.Add(this.nb_jour_essai);
            this.Contrat.Controls.Add(this.label14);
            this.Contrat.Location = new System.Drawing.Point(4, 22);
            this.Contrat.Name = "Contrat";
            this.Contrat.Padding = new System.Windows.Forms.Padding(3);
            this.Contrat.Size = new System.Drawing.Size(606, 424);
            this.Contrat.TabIndex = 5;
            this.Contrat.Text = "Contrat";
            this.Contrat.UseVisualStyleBackColor = true;
            // 
            // End
            // 
            this.End.Controls.Add(this.buttonImporter);
            this.End.Controls.Add(this.bt_Actualiser);
            this.End.Controls.Add(this.savePDF);
            this.End.Controls.Add(this.lookPDF);
            this.End.Location = new System.Drawing.Point(4, 22);
            this.End.Name = "End";
            this.End.Padding = new System.Windows.Forms.Padding(3);
            this.End.Size = new System.Drawing.Size(606, 424);
            this.End.TabIndex = 4;
            this.End.Text = "End";
            this.End.UseVisualStyleBackColor = true;
            // 
            // buttonImporter
            // 
            this.buttonImporter.Location = new System.Drawing.Point(41, 396);
            this.buttonImporter.Name = "buttonImporter";
            this.buttonImporter.Size = new System.Drawing.Size(75, 23);
            this.buttonImporter.TabIndex = 3;
            this.buttonImporter.Text = "Importer";
            this.buttonImporter.UseVisualStyleBackColor = true;
            this.buttonImporter.Click += new System.EventHandler(this.buttonImporter_Click);
            // 
            // bt_Actualiser
            // 
            this.bt_Actualiser.Location = new System.Drawing.Point(286, 396);
            this.bt_Actualiser.Name = "bt_Actualiser";
            this.bt_Actualiser.Size = new System.Drawing.Size(75, 23);
            this.bt_Actualiser.TabIndex = 2;
            this.bt_Actualiser.Text = "Actualiser";
            this.bt_Actualiser.UseVisualStyleBackColor = true;
            this.bt_Actualiser.Click += new System.EventHandler(this.bt_Actualiser_Click);
            // 
            // savePDF
            // 
            this.savePDF.Location = new System.Drawing.Point(490, 396);
            this.savePDF.Name = "savePDF";
            this.savePDF.Size = new System.Drawing.Size(84, 23);
            this.savePDF.TabIndex = 1;
            this.savePDF.Text = "Sauvegarder";
            this.savePDF.UseVisualStyleBackColor = true;
            this.savePDF.Click += new System.EventHandler(this.savePDF_Click);
            // 
            // lookPDF
            // 
            this.lookPDF.Location = new System.Drawing.Point(7, 7);
            this.lookPDF.Name = "lookPDF";
            this.lookPDF.Size = new System.Drawing.Size(593, 383);
            this.lookPDF.TabIndex = 0;
            this.lookPDF.Text = "";
            // 
            // nb_jour_essai
            // 
            this.nb_jour_essai.Location = new System.Drawing.Point(215, 18);
            this.nb_jour_essai.Maximum = new decimal(new int[] {
            7,
            0,
            0,
            0});
            this.nb_jour_essai.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nb_jour_essai.Name = "nb_jour_essai";
            this.nb_jour_essai.Size = new System.Drawing.Size(53, 20);
            this.nb_jour_essai.TabIndex = 5;
            this.nb_jour_essai.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(23, 20);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(186, 13);
            this.label14.TabIndex = 4;
            this.label14.Text = "Nombre de jours d\'acueil par semaine:";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(612, 450);
            this.Controls.Add(this.ongletEmployeur);
            this.Name = "Form1";
            this.Text = "Form1";
            this.onglet_Modification.ResumeLayout(false);
            this.onglet_Modification.PerformLayout();
            this.onglet_Employeur.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tab_Responsable1.ResumeLayout(false);
            this.tab_Responsable1.PerformLayout();
            this.tab_Responsable2.ResumeLayout(false);
            this.tab_Responsable2.PerformLayout();
            this.ongletEmployeur.ResumeLayout(false);
            this.Périodedessai.ResumeLayout(false);
            this.Périodedessai.PerformLayout();
            this.Contrat.ResumeLayout(false);
            this.Contrat.PerformLayout();
            this.End.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.nb_jour_essai)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.TabPage onglet_Modification;
        private System.Windows.Forms.TextBox dateNaiss_enfant;
        private System.Windows.Forms.TextBox prenom_enfant;
        private System.Windows.Forms.TextBox nom_enfant;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TabPage onglet_Employeur;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tab_Responsable1;
        private System.Windows.Forms.TextBox teltaf_responsable1;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox port_responsable1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox telfix_responsable1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox adresse_responsable1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox prenom_responsable1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox nom_responsable1;
        private System.Windows.Forms.TabPage tab_Responsable2;
        private System.Windows.Forms.TextBox teltaf_responsable2;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox port_responsable2;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox telfix_responsable2;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox adresse_responsable2;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox prenom_responsable2;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox nom_responsable2;
        private System.Windows.Forms.TabControl ongletEmployeur;
        private System.Windows.Forms.TabPage Périodedessai;
        private System.Windows.Forms.MonthCalendar cal_essai_debut;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TabPage End;
        private System.Windows.Forms.Button savePDF;
        private System.Windows.Forms.RichTextBox lookPDF;
        private System.Windows.Forms.Button bt_Actualiser;
        private System.Windows.Forms.Button buttonImporter;
        private System.Windows.Forms.TabPage Contrat;
        private System.Windows.Forms.NumericUpDown nb_jour_essai;
        private System.Windows.Forms.Label label14;
    }
}

