﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContratDeTravail
{
    class Enfant:Personne
    {
        private string date_naiss;

        public string Date_naiss { get => date_naiss; set => date_naiss = value; }

        /// <summary>
        /// COnstructeur par defaut d'un Enfant
        /// </summary>
        public Enfant()
            : base()
        {

        }

        public Enfant(string nom, string prenom, string date_naiss)
            :base()
        {
            Nom = nom;
            Prenom = prenom;
            this.date_naiss = date_naiss;
        }
    }
}
