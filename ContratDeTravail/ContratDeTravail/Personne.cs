﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContratDeTravail
{
    class Personne
    {
        private string nom;
        private string prenom;
        
        /// <summary>
        /// Getter et setter
        /// </summary>
        public string Nom { get => nom; set => nom = value; }
        public string Prenom { get => prenom; set => prenom = value; }

        /// <summary>
        /// Constructeur d'une personne par défaut
        /// </summary>
        public Personne()
        {
            Nom = "Inconnu";
            Prenom = "Inconnu";
        }

        /// <summary>
        /// Constructeur de la classe personne 
        /// </summary>
        /// <param name="nom"></param>
        /// <param name="prenom"></param>
        public Personne(string nom, string prenom)
        {
            this.nom = nom;
            this.prenom = prenom;
        }
    }
}
