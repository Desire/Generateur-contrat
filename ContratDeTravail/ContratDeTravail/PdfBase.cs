﻿using System;
using System.Drawing;
using System.IO;
using iTextSharp.text;
using iTextSharp.text.pdf;

namespace ContratDeTravail
{
    class PdfBase
    {
        public static void PdfBase1()
        {
            //Mise en place des objets utiliser 
            Personne responsable1 = new Parents_Employeurs(Form1.nomResp1, Form1.prenomResp1, Form1.adresseResp1, Form1.telFixeResp1, Form1.portResp1, Form1.telTafResp1);
            Personne responsable2 = new Parents_Employeurs(Form1.nomResp2, Form1.prenomResp2, Form1.adresseResp2, Form1.telFixeResp2, Form1.portableResp2, Form1.telTafResp2);
            Personne Asmat = new Ass_mat("Patureau", "Christelle", "11 rue des fosses", "0243944271", "0640704590");
            Personne Enfant = new Enfant(Form1.nomEnfant,Form1.prenomEnfant,Form1.datNaissEnfant);

            string fichier = "test.pdf";
            Document doc = new Document();
            try
            {
                PdfWriter.GetInstance(doc, new FileStream( fichier , FileMode.Create));
                doc.Open();

                //Mise en page du pdf

                doc.Add(new Phrase("ENGAGEMENT RECIPROQUE \n"));
                doc.Add(new Phrase("\nLes futurs employeur et salarié peuvent se mettre d’accord sur le principe de la conclusion à un moment donné, d’un contrat de travail relatif à l’accueil d’un enfant. \n"));
                doc.Add(new Phrase("\nSuite au contact pris ce jour : " + DateTime.Now.ToShortDateString()));
                doc.Add(new Phrase("\n\nEntre\nLe parent employeur: "));
                doc.Add(new Phrase("\nNom : " + responsable1.Nom));
                doc.Add(new Phrase("\nPrenom : " + responsable1.Prenom));
                doc.Add(new Phrase("\nAdresse : " + responsable1.Adresse));
                doc.Add(new Phrase("\nTelephone fixe : " + responsable1.Telephone));
                doc.Add(new Phrase("\nPortable : " + responsable1.Portable));
                doc.Add(new Phrase("\nTelephone professionnel : " + responsable1.Tel_taf));
                doc.Add(new Phrase("\n\nL’autre parent (non employeur) :"));
                doc.Add(new Phrase("\nNom : " + responsable2.Nom));
                doc.Add(new Phrase("\nPrenom : " + responsable2.Prenom));
                doc.Add(new Phrase("\nAdresse : " + responsable2.Adresse));
                doc.Add(new Phrase("\nTelephone fixe : " + responsable2.Telephone));
                doc.Add(new Phrase("\nPortable : " + responsable2.Portable));
                doc.Add(new Phrase("\nTelephone professionnel : " + responsable2.Tel_taf));
                doc.Add(new Phrase("\n\nEt le ou la salarié(e), en qualité d’assistant(e) maternel(le) :"));
                doc.Add(new Phrase("\nNom : " + Asmat.Nom));
                doc.Add(new Phrase("\nPrenom : " + Asmat.Prenom));
                doc.Add(new Phrase("\nAdresse : " + Asmat.Adresse));
                doc.Add(new Phrase("\nTelephone fixe : " + Asmat.Telephone));
                doc.Add(new Phrase("\nPortable : " + Asmat.Portable));
                doc.Add(new Phrase("\n\nPour l’accueil de l’enfant: "));
                doc.Add(new Phrase("\nNom : " + Enfant.Nom));
                doc.Add(new Phrase("\nPrenom : " + Enfant.Prenom));
                doc.Add(new Phrase("\nDate de naissance : " + Enfant.Date_Naiss));
            }
            catch (DocumentException de)
            {
                Console.WriteLine("error" + de.Message);
            }
            catch (IOException ioe)
            {
                Console.WriteLine("error" + ioe.Message);
            }
            doc.Close();
        }
    }
}