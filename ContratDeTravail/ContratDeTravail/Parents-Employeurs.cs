﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContratDeTravail
{
    class Parents_Employeurs:Personne
    {
        private string adresse;
        private string tel_fixe;
        private string portable;
        private string tel_taf;

        /// <summary>
        /// Getter et Setter
        /// </summary>
        public string Adresse { get => adresse; set => adresse = value; }
        public string Tel_fixe { get => tel_fixe; set => tel_fixe = value; }
        public string Portable { get => portable; set => portable = value; }
        public string Tel_taf { get => tel_taf; set => tel_taf = value; }

        /// <summary>
        /// Constructeur d'un Parents_Employeur par defaut
        /// </summary>
        public Parents_Employeurs()
            :base()
        {

        }

        /// <summary>
        /// Constructeur de la classe Parents_Employeurs
        /// </summary>
        /// <param name="adresse"></param>
        /// <param name="tel_fixe"></param>
        /// <param name="portable"></param>
        /// <param name="tel_taf"></param>
        public Parents_Employeurs(string nom, string prenom, string adresse, string tel_fixe, string portable, string tel_taf)
            :base()
        {
            Nom = nom;
            Prenom = prenom;
            this.adresse = adresse;
            this.tel_fixe = tel_fixe;
            this.portable = portable;
            this.tel_taf = tel_taf;
        }
    }
}
